# nvr - Network Video Recorder

These bash scripts implement video capture and analsysis of IP-cameras, including motion-detection. 

See nvr_capture.sh for details on prerequisites (ffmpeg, dvr-scan, ... ) and configuration. 

Usage:
    nvr_capture.sh  camera_username  camera_password
