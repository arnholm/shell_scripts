#!/bin/bash
#
# capture/process video from rtsp video stream using ffmpeg, dvr-scan
#
# usage:
#   nvr_capture <username>  <password>
# 
# prerequisites:
#
#   nvr_capture.sh
#   --------------
#      - ffmpeg installed
#      - IP camera with RTSP support, tested with TP-Link Tapo C310
#   
#   nvr_process.sh
#   --------------
#      - Python 3.12 virtual environment (below is for ubuntu 22.04)
#           sudo add-apt-repository ppa:deadsnakes/ppa
#           sudo apt update
#           sudo apt install python3.12
#           sudo apt install python3.12-venv
#           /ssd1/venv$ python3.12 -m venv py312
#
#      - dvr-scan ( https://www.dvr-scan.com/ )
#           (py312) pip install dvr-scan
#           (py312) pip install opencv-python
#
# Author:
#   Copyright 2023 Carsten Arnholm
#
# License: 
#     MIT License 
#     https://mit-license.org/
#
SCRIPT=$(realpath "$0")
script_dir=$(dirname "$SCRIPT")
#
cam_user="$1"                          # credential: camera username
cam_pw="$2"                            # credential: camera password
#
# =========================
# User configuration begins
# =========================
#
download_dir="/ssd1/nvr_capture"       # where to capture videos (before processing)
process_dir="${download_dir}/process"  # captured videos are moved here for processing
events_dir="/NAS_scratch/nvr_events"   # videos with events are moved here after processing
#
cam_ip="10.0.0.115:554"                              # IP-address:port for camera in network
cam_path="stream1"                                   # final part of RTSP url path (depends on camera used)
cam_name="cam1"                                      # a name to attach to the stored files
cam_fps=10.0                                         # frames per second
segments=10                                          # number of video segment files per capture
seg_time=60                                          # seconds per segment
use_regions=true                                     # true if regions to be applied
regions="0 720 0 205 145 157 244 5 1280 0 1280 720"  # region is scene and resolution dependent, here 1280x720
#
# =======================
# User configuration ends
# =======================
#
let capture_time=${seg_time}*${segments}     # length of batch capture in seconds
let timeout_segs=${segments}+1               # make timout equivalent to one segment delay
let timout_time=${seg_time}*${timeout_segs}  # timeout limit for batch capture
#
# run from download dir
mkdir -p "${download_dir}"
cd ${download_dir}
echo "capturing from: ${cam_ip}"
echo "capturing to  : ${download_dir}"
echo "event files in: ${events_dir}/yyyymmdd"
#
# https://stackoverflow.com/questions/20071030/how-to-capture-ctrl-c-and-use-it-to-exit-an-endless-loop-properly
EXIT=0
exiting() { echo "Ctrl-C trapped, exiting" ; EXIT=1;}
trap exiting SIGINT

# https://stackoverflow.com/questions/9954794/execute-a-shell-function-with-timeout
# ref Tiago Lopo answer. This allows synchronous call to a shell function or 
# external command, with a timeout in case something hangs
function run_cmd { 
    cmd="$1"; timeout="$2";
    grep -qP '^\d+$' <<< $timeout || timeout=10

    ( 
        eval "$cmd" &
        child=$!
        trap -- "" SIGTERM 
        (       
            sleep $timeout
            kill $child 2> /dev/null 
        ) &     
        wait $child
    )
}

# capture a sequence of video segments
capture_video_segments() {
    if  [ $EXIT -eq 0 ]; then
        #
        # -filter:v scale=1280:-1     : scales the video image size to width=1280
        # -fpsmax 10.0                : sets the frame rate to 10 frames per second
        # -map 0                      : video stream selection: record all streams
        # -t 600                      : stop recording after 600 seconds
        # -f segment                  : Run in loops creating one MP4 per time segment
        # -segment_time 60            : Each time segment 60 seconds
        # -reset_timestamps 1         : Reset time for each time segment file
        # -segment_format mp4         : Use MP4 output format
        # -b:v 4000k                  : video bitrate
        # -strftime 1                 : generate date/time data for file name generation
        #
        # echo "Capturing $(date +%Y%m%d_%H%M%S), segs=${segments} segtime=${seg_time}s : ${download_dir}"
        ffmpeg -loglevel error -hide_banner -nostats \
            -i rtsp://${cam_user}:${cam_pw}@${cam_ip}/${cam_path} \
            -filter:v scale=1280:-1 \
            -fpsmax ${cam_fps} \
            -map 0 \
            -t ${capture_time} \
            -f segment -segment_time ${seg_time} -reset_timestamps 1 -segment_format mp4 \
            -b:v 4000k \
            -strftime 1 \
            "${cam_name}_%Y%m%d_%H%M%S.mp4"
    fi  
}


while [ $EXIT -eq 0 ]
do 
    # capture videos using ffmpeg segments 
    # ====================================
    # this runs in a separate, synchronous subprocess.
    # If timeout_time is exceeded, the subprocess is killed
    run_cmd "capture_video_segments" ${timout_time}

    # process videos using dvr-scan
    # =============================
    # process in the background in "fire and forget" mode
    if  [ $EXIT -eq 0 ]; then
        # move the captured *.mp4 files to ${process_dir} 
        mkdir -p "${process_dir}"
        mv -f *.mp4 "${process_dir}"
        if [ $use_regions ]; then
            echo ${regions} > "${process_dir}/regions.txt"
        else
            rm -f "${process_dir}/regions.txt"
        fi
        YYMMDD=$(date +%Y%m%d)
        cmd="${script_dir}/nvr_process.sh ${process_dir} ${events_dir}/${YYMMDD} "
        eval "$cmd" &
    fi
done

