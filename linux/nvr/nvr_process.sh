#!/bin/bash
#
# This script is called from nvr_capture.sh
# It processes MP4 videos using dvr-scan, ref. https://www.dvr-scan.com/ 
#
# Author:
#   Copyright 2023 Carsten Arnholm
#
# License: 
#     MIT License 
#     https://mit-license.org/
#
# activate the Python environment that contains dvr-scan
source /ssd1/venv/py312/bin/activate

# go to the processing folder
process_dir="$1"
events_dir="$2"
#
echo "Processing $(date +%Y%m%d_%H%M%S)"
#
# check for possible existence of "regions.txt" file and apply if present
regions_option=""
regions_file="${process_dir}/regions.txt"
if [ -f ${regions_file} ]; then
    regions_option="-R ${regions_file}"
    echo "   using regions: ${regions_option}"
fi

pushd ${process_dir} > /dev/null

    # create empty arrays
    declare -a MP4rm=()    # files to be removed
    declare -a MP4mv=()    # files with events, to be moved to "events" folder

    for f in *.mp4
    do
        echo -n "   $f: "
       
        scan_file="$f.txt"
        # --quiet will generate ${scan_file} with size 0 if no event is found
        # --kernel-size 15 blurs out image noise and therefore eliminates some false positives
        # -fs 3 processes every 3rd frame, saves processing time
        # -l 1.5s require events to last at least 1.5s and therefore eliminates some false positives
        # -R ${regions_file} limits detection region  and therefore eliminates some false positives
        dvr-scan -i "$f" --quiet --scan-only --kernel-size 15 ${regions_option} -fs 3 -l 1.5s > ${scan_file}

        if [ -z "$(cat ${scan_file})" ]; then
            # scan file is empty, no event found.
            # this file shall be deleted
            MP4rm+=("$scan_file")
            MP4rm+=("$f")
            echo "no events"
        else
            # motion event(s) were detected
            MP4mv+=("$scan_file")
            MP4mv+=("$f")
            echo -n "event(s) at "
            cat ${scan_file}
        fi       
    done
    
    # save the videos with events in the events folder, along with event ${scan_file}
    mkdir -p "${events_dir}"
    for f in "${MP4mv[@]}"
    do
        mv -f "$f" "${events_dir}/"
    done
    
    # delete non-event files
    for f in "${MP4rm[@]}"
    do
        rm -f "$f"
    done
    
echo "Processing done"
popd > /dev/null
