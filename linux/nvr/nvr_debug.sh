#!/bin/bash
#
# This script is used for fine-tuning dvr-scan options
# it processes *.mp4 files found in ${process_dir}, but saves nothing
#
# Author:
#   Copyright 2023 Carsten Arnholm
#
# License: 
#     MIT License 
#     https://mit-license.org/
#
# activate the Python environment that contains dvr-scan
source /ssd1/venv/py312/bin/activate

process_dir="/ssd1/nvr_capture/debug"

# check for possible existence of "regions.txt" file and apply if present
regions_option=""
regions_file="${process_dir}/regions.txt"
if [ -f ${regions_file} ]; then
    regions_option="-R ${regions_file}"
    echo "using regions: ${regions_option}"
fi

pushd ${process_dir} > /dev/null

    for f in *.mp4
    do
        echo -n "$f  "
        
        scan_file="$f.txt"
        # --quiet will generate ${scan_file} with size 0 if no event is found
        dvr-scan -i "$f" --quiet --kernel-size 15 ${regions_option} -fs 3 -l 1.5s > ${scan_file}

        if [ -z "$(cat ${scan_file})" ]; then
            echo "0"
        else
            cat ${scan_file}
        fi  
        rm ${scan_file}
    done
    
popd > /dev/null
